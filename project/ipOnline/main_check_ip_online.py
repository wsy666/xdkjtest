# -*- coding: utf-8 -*-
'''
@File    :   widget_check_ips.py
@Time    :   2022/03/20 14:44:28

'''
import re
from functools import partial

# import PyQt5.QtCore as PQC
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtCore import pyqtSlot, QObject, QTimer
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QApplication, QPushButton, QListWidgetItem
, QListWidget,
                             QMenu)
from log import logger

from project import NetWorkCard, Card, set_pc_ips

from project.ipOnline.pack.middle import GlobalDataUi
from project.ipOnline.pack.ping_ip import ManageTheads
from project.ipOnline.pack.standard_model import ContainerInListModel
from project.ipOnline.ui import mtab
from .pack.middle import check_edit_text
from .ui.ip_online import Form
from ..pack.ip import IpState
from .pack.ping_ip import ManageTheadSustain

logger.info("")


class ProxyNetWorkCard:
    """
   代理NetWorkCard 连接 视图,作为中间件
    
    """
    
    def __init__(self):
        self.NetWorkCard = NetWorkCard()
        self.card: Card = self.NetWorkCard.get_card_from_name()
        if self.card is None:
            self.card = Card()
    
    def switch_current_card(self, card_name):
        """
        切换网卡
        :return True or None
        
        """
        card = self.NetWorkCard.get_card_from_name(card_name)
        if card:
            self.card = card
            logger.debug("切换网卡成功, 当前网卡名称: {}".format(card_name))
            return True
    
    def get_cardname_list(self):
        """
        获取网卡的名称列表
        :return  [card_name...]
        """
        cards_name_list = list(self.NetWorkCard.get_card_names())
        curr_card_name = self.card.name
        if curr_card_name in cards_name_list:
            cards_name_list.remove(curr_card_name)
        
        # 放在列表的首位
        cards_name_list.insert(0, curr_card_name)
        
        return cards_name_list
    
    def add_and_set_pc_ip(self, ip):
        """
        向本身IP添加一个IP
        """
        ips = self.card.ips()
        ips.append(ip)
        
        logger.debug("当前Card要增加的IP是:{}".format(ip))
        rev = set_pc_ips(ips, self.card)
        return rev


from project import IP


class ProxyListWidget(QObject):
    btnClickSignal = pyqtSignal(str)
    
    def __init__(self, listwidget: QListWidget, parent=None):
        super(ProxyListWidget, self).__init__()
        
        # text: [点击查找 [4] 网段IP]
        self.btn_texts = []
        self.parent = parent
        self.ListWidget = listwidget
    
    def create_items_from_card(self, card: Card):
        """
        向ListWidget添加所有的项,带btn控件;
        """
        ips_list = card.ips()
        
        if ips_list:
            self.ListWidget.clear()
            self.btn_texts = []
        
        for ip_str in ips_list:
            self.add_section(ip_str)
    
    def add_section(self, ip_str):
        """
        ListWidget , 跟据IP字段, 添加一行(项)
        """
        if IP.check_ip_formatter(ip_str) is None:
            return
        
        ipobj = IP(ip_str)
        text = "点击查看[ {} ]网段在线状态"
        text = text.format(ipobj.section())
        self.btn_texts.append(text)
        self.__create_item(text)
        logger.debug("ListWidget add item, text is {}".format(text))
    
    def __remove_current_items(self):
        """
        删除当前选中的一行;
        未使用
        """
        index = self.ListWidget.currentIndex()
        item_text = self.ListWidget.currentItem().text()
        row = index.row()
        self.ListWidget.takeItem(row)
        self.btn_texts.remove(str(item_text))
        logger.info("ListWidget take current item: {}".format(item_text))
    

    
    def __create_item(self, btn_text):
        """
        跟据 提供的文本 向ListWidget添加一行;
        """
        btn = QPushButton(btn_text, self.parent)
        item = QListWidgetItem()
        self.ListWidget.addItem(item)
        self.ListWidget.setItemWidget(item, btn)
        
        # 连接按钮的槽
        btn.clicked.connect(partial(self.on_click, btn_text))
    
    @staticmethod
    def __analy_item_text(text: str):
        """
        解析如下格式的文字
       `点击查找 [{}] 网段的IP`
       :return
            ip->section <str>
        """
        match = re.search(r"\d+", text)
        if match:
            return match.group()
    
    def on_click(self, btn_text):
        """
        列表中每个按钮共有的槽函数;
        """
        logger.debug("slot param is: {}".format(btn_text))
        tsr = ""
        section = self.__analy_item_text(btn_text)
        self.btnClickSignal.emit(section)
        self.set_btn_status(False)
        
    
    def set_btn_status(self, b_enable = True):
        """
        b_enable: True 默认
        设置btn的状态
        """
        b_enable = bool(b_enable)
        count_ = self.ListWidget.count()
        for row in range(count_):
            item = self.ListWidget.item(row)
            btn = self.ListWidget.itemWidget(item)
            btn.setEnabled(b_enable)
            
        


class MyClass(Form):
    """
   ip_one_line的主窗口
   QDialog
    
    """
    _startThread = pyqtSignal()
    
    def __init__(self):
        super(MyClass, self).__init__()
        
        # data
        self.manage_threads = ManageTheads()
       
        #  好像是配置文件
        self.gdata = GlobalDataUi()
        # model
        self.model = ContainerInListModel()
        
        #
        self.proxy_network = ProxyNetWorkCard()
        
        #
        self.proxy_list_widget = ProxyListWidget(self.sectionListWidget, self)
        self.proxy_list_widget.btnClickSignal[str].connect(self.__on_checkip_range)
        
        
        self.init_tableview_sustain()
        self.init()
        
        # timer
        self.timer = QTimer()
        self.timer.timeout.connect(self.modelSustain.flush)
        
        # 右键菜单
        self.tableViewSustain.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tableViewSustain.customContextMenuRequested.connect(self.__generateMenu)
        
        #
        self.lineLineIp.setText("192.168.10.100")
        self.lineLineIpEnd.setText("192.168.10.106")
        
        
    def init_tableview_sustain(self):
        """
        设置另外一个表格
        持续检测IP的在线状态
        """
        self.modelSustain = ContainerInListModel()
        self.tableViewSustain.setModel(self.modelSustain)
        mtab.set_header(self.modelSustain)
        self.tableViewSustain.setColumnWidth(0, 50)
        
        # datas
        self.mthreadSustain = ManageTheadSustain()

        
        pass
    
    
    def init(self):
        """
        初始化控件
        
        """
        print('init')
        
        self.change_line_and_btn_state(True)
        
        self.progressBar.setValue(0)
        
        self.push_test.hide()
        
        self.tableview.setColumnWidth(0, 50)
        self.tableview.setModel(self.model)
        
        # 设置表格
        mtab.set_header(self.model)
        
        # card combo
        self.comboBox.addItems(self.proxy_network.get_cardname_list())
        
        # init sectionListWidget
        card = self.proxy_network.card
        self.proxy_list_widget.create_items_from_card(card)
    
    # def add_one_row_control(self):
    #     """
    #    添加一行查找ip的控件
    #     """
    
    @staticmethod
    def line_edit_auto_text(edit_s, edit_e):
        if not edit_s.hasFocus():
            return
        text = edit_s.text()
        try:
            result = check_edit_text(text, flag='start')
            if isinstance(result, tuple):
                edit_s.setText(result[0])
                edit_e.setText(result[1])
            else:
                edit_s.setText(result[0])
        except ValueError:
            pass
    
    
    
    def __generateMenu(self, pos):
        """
        tableViewSustain  表格 生成右键菜单函数
        
        """
        if self.focusWidget() == self.tableViewSustain :
            menu = QMenu()
            # item_del = menu.addAction(QIcon("data\\menu_del.ico"), "关闭检测")
            item_close = menu.addAction( "关闭检测")
            item_open = menu.addAction( "开启检测")
            item_del = menu.addAction( "删除IP")
            item_close_section = menu.addAction( "关闭此网段的IP检测")
            
            index = self.tableViewSustain.currentIndex()
            cur_ipstr = self.modelSustain.data(index)
            logger.debug("当前右键选择的IP是: {}".format(cur_ipstr))
            action = menu.exec_(self.tableViewSustain.mapToGlobal(pos))

            if action == item_close:
                self.modelSustain.change_ip_online_status(cur_ipstr, False)
                self.modelSustain.flush()
                pass
            elif action == item_open:
                self.modelSustain.change_ip_online_status(cur_ipstr, True)
                self.modelSustain.flush()
                pass
            
            elif action == item_del:
                rev_ipate = self.modelSustain.remove_ipstate(cur_ipstr)
                if rev_ipate:
                    self.mthreadSustain.quit_one(rev_ipate)
                self.modelSustain.flush()

            elif action == item_close_section:
                _co_list : list = self.modelSustain.remove_section_ipstates(cur_ipstr)
                for ipstate in _co_list:
                    self.mthreadSustain.quit_one(ipstate)
                self.modelSustain.flush()

    
    @pyqtSlot()
    def on_push_test_clicked(self):
        """
        测试按扭
        """
        print('on_test')
        start, end = self.get_ip_from_lineedit(self.btn_search1)
        self._set_current_section(start)
        self.model.flush()
        
        self.model.current_section = '8'
        from project.ipOnline.pack.standard_model import factory_container_model_obj
        factory_container_model_obj(self.model)
        pass
    
    @pyqtSlot()
    def on_btn_clear_clicked(self):
        """
        清空表格
        """
        self.model.clear()
        
        pass
    
    # -------------------------------------------------------------
    # 结束
    # -------------------------------------------------------------
    
    def _set_current_section(self, ip):
        sec = IpState(ip).section()
        print("sec is {}".format(sec))
        self.model.switch_section(sec)
    
    def init_ping_ip(self, start, end):
        self.manage_threads.create_threads(start, end)
        self.manage_threads.manage_signal_finishedall.connect(self.slot_finished_all_thread)
        self.manage_threads.manage_signal_send_onlineip.connect(self.slot_receive_ip)
        self.manage_threads.manage_signal_oneth_end[int, int].connect(self.update_progressbar)
    
    def __init_ping_ip(self, ips: list):
        self.manage_threads.create_threads(ips=ips)
        self.manage_threads.manage_signal_finishedall.connect(self.slot_finished_all_thread)
        self.manage_threads.manage_signal_send_onlineip.connect(self.slot_receive_ip)
        self.manage_threads.manage_signal_oneth_end[int, int].connect(self.update_progressbar)
    
    def slot_receive_ip(self, ip):
        """
        singal信号的槽
        作用:设置一个在线IP 所对应的IP的 背景颜色
        """
        logger.debug("success ping : {}".format(ip))
        self.model.add_ip_update_all_model(ip)
    
    def set_source_two_ip(self, btn, start, end):
        if btn == self.btn_search1:
            self.gdata.set_start_end1(start, end)
        else:
            self.gdata.set_start_end2(start, end)
    
    def __on_checkip_range(self, section: str):
        
        combin_ip_str = "192.168.{}.2".format(section)
        create_ip = IP(combin_ip_str)
        ips = list(create_ip.generate_2_254_ips())
        
        self.progressBar.setValue(0)
        self.model.flush()
        self.__init_ping_ip(ips)
        self.manage_threads.start()
    
    def slot_finished_all_thread(self):
        """
        所有的ip ping完毕
        """
        print("收到信号,执行槽 finished_all_ths")
        # self._enable_btn(True)
        # 结束所有的th
        self.manage_threads.quit()
        self.progressBar.setValue(100)
        
        # no on line
        self.model.update_not_online()
        self.model.flush()
        # 设置ListWidget里面的btn的enable状态
        self.proxy_list_widget.set_btn_status(True)
    
    @pyqtSlot()
    def on_btnAddSection_clicked(self):
        """
        添加电脑当前网卡的网段
        """
        logger.info("开始添加一个PC当前网卡的网段")
        self.change_line_and_btn_state(False)
    
    @pyqtSlot()
    def on_btnSectionAddSure_clicked(self):
        """
        确定添加输入的网段
        """
        section_: str = self.lineSection.text()
        if not section_.isalnum():
            # 如果字符串不是数字,就返回
            logger.warning("输入的网段不是数字")
            return
        
        PC_TAIL = '151'
        ip_tsr = "192.168.{}.{}".format(section_, PC_TAIL)
        
        # 本机添加一个IP段
        rev = self.proxy_network.add_and_set_pc_ip(ip_tsr)
        if rev is None:
            # 设置本机不成功
            self.lineSection.setText("")
            self.change_line_and_btn_state(True)
            logger.warning("添加本要IP不成功~")
            return
        
        # QListWidget列表添加一行
        self.proxy_list_widget.add_section(ip_tsr)
        self.change_line_and_btn_state(True)
        logger.info("添加PC 当前网卡的一个网段成功")
    
    @pyqtSlot(str)
    def on_comboBox_currentIndexChanged(self, p0):
        """
        切换当前的网卡
        @param p0 DESCRIPTION
        @type str
        """
        
        card_name = p0
        self.proxy_network.switch_current_card(card_name)
        
        card = self.proxy_network.card
        self.proxy_list_widget.create_items_from_card(card)
    

    @pyqtSlot()
    def on_btnSustainStart_clicked(self):
        """
        Slot documentation goes here.
        """
        logger.debug("开始持续的ping所有的有效IP...")
        self.mthreadSustain = ManageTheadSustain()
        ipstate_list = self.modelSustain.get_curr_ipstate_list()
        self.mthreadSustain.create_threads(ipstate_list)
        self.mthreadSustain.start()
        self.timer.start(1000)
    
    @pyqtSlot()
    def on_btnSustainStop_clicked(self):
        """
        Slot documentation goes here.
        """
        logger.debug("所在持续ping IP线程停止")
        self.mthreadSustain.quit()
        self.timer.stop()
        


    def change_line_and_btn_state(self, b_status):
        
        b_status = bool(b_status)
        self.lineSection.setHidden(b_status)
        self.btnSectionAddSure.setHidden(b_status)
    
    def update_progressbar(self, finished_ths, sum_ths):
        # print(finished_ths, sum_ths)
        value = int(100 * finished_ths / sum_ths)
        self.progressBar.setValue(value)
    
    def closeEvent(self, a0) -> None:
        print('closeEvent')
        self.gdata.save_cfg()
    
    @pyqtSlot()
    def on_btnFlushIp_clicked(self):
        """
        添加新的字段后,手动刷新列表
        """
        
    @pyqtSlot()
    def on_btnAddSustain_clicked(self):
        """
        添加IP到持续检测表格
        """
        logger.debug("QPushButton: AddSustain 添加IP到持续检测表格")
        start = self.lineLineIp.text()
        end = self.lineLineIpEnd.text()
        logger.debug("QLineEdit start ip is :{}, end ip is {}".format(start, end))
        
        if IP.check_ip_formatter(end) is None:
            # 将只选取start ip, 单一的IP
            end = None
        
        else:
            end = IP(end)
            
        if IP.check_ip_formatter(start) is None:
            start = None
        else:
            start = IP(start)
            
        #
        if start  and end is None:
            self.modelSustain.add_ip_update_all_model(start.ip)
            logger.debug("表格sustain添加IP {}".format(start.ip))
            return
        
        if start is None and end:
            self.modelSustain.add_ip_update_all_model(end.ip )
            logger.debug("表格sustain添加IP {}".format(end.ip))
            return
        
        if start and end:
            start_tail_int = int(start.tail())
            end_tail_int = int(end.tail())
            
            if start.header_3() != end.header_3():
                # 两个IP的前面三部分不一样
                self.modelSustain.add_ip_update_all_model(start.ip)
                self.modelSustain.add_ip_update_all_model(end.ip)
                logger.debug("表格sustain添加IP {}".format(start.ip))
                logger.debug("表格sustain添加IP {}".format(end.ip))
                
            elif start < end:
                for tail in range(start_tail_int, end_tail_int+1):
                    new_ip_str = start.replace_tail(tail)
                    self.modelSustain.add_ip_update_all_model(new_ip_str)
                    logger.debug("表格sustain添加IP {}".format(new_ip_str))
            else:
                for tail in range(end_tail_int, start_tail_int+1):
                    new_ip_str = start.replace_tail(tail)
                    self.modelSustain.add_ip_update_all_model(new_ip_str)
                    logger.debug("表格sustain添加IP {}".format(new_ip_str))


        
                

        
            
        
        
        
        
        
def main():
    # PyQt / Qt解决分辨率不同的设备显示问题
    # QCoreApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    import sys
    app = QApplication(sys.argv)
    win = MyClass()
    win.push_test.hide()
    win.show()
    sys.exit(app.exec_())

# if __name__ == '__main__':
#     main()
