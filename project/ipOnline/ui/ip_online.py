# -*- coding: utf-8 -*-

"""
Module implementing Form.
"""

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QWidget

from .Ui_ip_online import Ui_Form


class Form(QWidget, Ui_Form):
    """
    Class documentation goes here.
    """
    def __init__(self, parent=None):
        """
        Constructor
        
        @param parent reference to the parent widget
        @type QWidget
        """
        super(Form, self).__init__(parent)
        self.setupUi(self)
    
        
    
    @pyqtSlot()
    def on_push_test_clicked(self):
        """
        Slot documentation goes here.
        """
        
        
    
    @pyqtSlot()
    def on_btn_clear_clicked(self):
        """
        Slot documentation goes here.
        """
        

    @pyqtSlot(str)
    def on_comboBox_currentIndexChanged(self, p0):
        """
        Slot documentation goes here.
        
        @param p0 DESCRIPTION
        @type str
        """
        
        
    
    @pyqtSlot()
    def on_btnAddSection_clicked(self):
        """
        Slot documentation goes here.
        """
    
    @pyqtSlot()
    def on_btnSectionAddSure_clicked(self):
        """
        Slot documentation goes here.
        """
    
    @pyqtSlot()
    def on_btnFlushIp_clicked(self):
        """
        Slot documentation goes here.
        """
    
    @pyqtSlot()
    def on_btnAddSustain_clicked(self):
        """
        添加IP到持续检测表格
        """
    
    @pyqtSlot()
    def on_btnSustainStart_clicked(self):
        """
        Slot documentation goes here.
        """

    @pyqtSlot()
    def on_btnSustainStop_clicked(self):
        """
        Slot documentation goes here.
        """
