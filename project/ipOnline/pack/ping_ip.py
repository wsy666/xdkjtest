# -*- coding: utf-8 -*-
'''
@File    :   ping_ip.py
@Time    :   2022/03/20 18:30:25

'''

import subprocess
import time

from PyQt5.QtCore import QObject, QThread, pyqtSignal
from log import logger
from ping3 import ping

from project import IpState
from ...pack.ip import _get_range_ips


class PingIp(QObject):
    """
    # 不用了,速度太慢了
    
    """
    signal_ping_send_ip = pyqtSignal(str)
    signal_ping_check_end = pyqtSignal()
    
    def __init__(self, ip):
        super(PingIp, self).__init__()
        self.ip = ip
    
    def run(self):
        p = subprocess.Popen(['ping.exe', self.ip], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                             shell=False)
        res = p.stdout.readlines()
        for line in res:
            if 'TTL' in line.decode('gbk'):
                logger.info("IP {} is online".format(self.ip))
                self.signal_ping_send_ip.emit(self.ip)
                break
        else:
            self.signal_ping_check_end.emit()


class PingIpBase(QObject):
    
    def __init__(self, ip_obj: IpState):
        super(PingIpBase, self).__init__()
        self.ip_obj = ip_obj
        
    @property
    def ip(self):
        return self.ip_obj.ip
    
    def run(self):
        pass
    
        
        

class PingIp3(QObject):
    """
    ping IP
    :argument ip  : str

    """
    signal_ping_send_ip = pyqtSignal(str)
    
    def __init__(self, ip):
        """ """
        super(PingIp3, self).__init__()
        self.ip = ip
    
    def run(self):
        code = ping(self.ip, timeout=2) # todo config timeout
        if code:
            self.signal_ping_send_ip.emit(self.ip)
        else:
            # self.signal_ping_check_end.emit()
            self.signal_ping_send_ip.emit("")


class PingIp3While(QObject):
    signal_pingip_while = pyqtSignal()
    def __init__(self, ip_obj, timesleep=0.8, stop_command=False):
        
        super(PingIp3While, self).__init__()
        self.ip_obj = ip_obj
        self.timesleep = timesleep
        self.stop_command = stop_command
    
    @property
    def ip(self):
        return self.ip_obj.ip
        
    def set_stop(self):
        self.stop_command = True
    
    
    def run(self):
        logger.debug("run function is run, ip - {}".format(self.ip))
        while 1 == 1:
            if self.ip_obj.state != IpState.STATE_FINDED:
                code = ping(self.ip, timeout=2)
                if code:
                    # 在线ip, 设置为绿色
                    logger.debug("ip - {} online, status is <new>".format(self.ip))
                    self.ip_obj.set_new()
                else:
                    # 不在线ip, 设置为白色
                    self.ip_obj.set_unknow()
                    logger.debug("IP - {} 不在线".format(self.ip))

            time.sleep(self.timesleep)
    
            if self.stop_command:
                logger.info("线程停止运行,ping ip is : {}, break".format(self.ip))
                break
                
            self.signal_pingip_while.emit()
            
            


class ManageTheads(QObject):
    manage_signal_finishedall = pyqtSignal()  # 所有线程结束时发送
    manage_signal_send_onlineip = pyqtSignal(str)  # ping 通IP时发送
    manage_signal_oneth_end = pyqtSignal(int, int)  # 当一个线程结束时产生, 发送当前已经结束的线程数和总的线程数
    
    num_finished_threads = 0
    
    def __init__(self):
        super(ManageTheads, self).__init__()
        self.ping_objs = []
        self.ths = []
    
    def create_threads(self, start=None, end=None, ips=None):
        if len(self.ths) > 0:
            self.ths = []

        if ips is not None and isinstance(ips, list):
            ips = ips
        else:
            ips = _get_range_ips(start, end)

        self.__create_threads_from_scope(ips)
        
        logger.info("创建的线程数量为:  {}".format(self.len_threads()))

    def __create_threads_from_scope(self, ips):
        for ip in ips:
            ping_obj = PingIp3(ip)
            # ip_obj = PingIp(ip)
            th = QThread()
    
            ping_obj.moveToThread(th)
            th.started.connect(ping_obj.run)
        
            # ip_obj.signal_ping_check_end.connect(self.slot_finised_thread)
            ping_obj.signal_ping_send_ip[str].connect(self.slot_send_ip)
        
            self.ths.append(th)
            self.ping_objs.append(ping_obj)
            

    def slot_send_ip(self, ip):
        """
        当ip在线时,所发信号的糟函数
        当ip为"" 时,说明没有检测到;
        """
        if ip:
            self.manage_signal_send_onlineip.emit(ip)
        
        self.num_finished_threads += 1
        
        self.manage_signal_oneth_end.emit(self.num_finished_threads, self.len_threads())
        print("manage_signal_oneth_end emit-> args is {}, {}".format(self.num_finished_threads, self.len_threads()))
        
        if self.num_finished_threads >= len(self):
            # 每检测一点,此信号只发送一次
            self.manage_signal_finishedall.emit()
            print("全部线程已经运行完成,发射信号:singal_all_thread_finished")
            self.num_finished_threads = 0
    
    def quit(self):
        for th in self.ths:
            th.quit()
    
    def start(self):
        for th in self.ths:
            th.start()
    
    def len_threads(self):
        return len(self.ths)
    
    def __len__(self):
        return len(self.ths)


class ManageTheadSustain(QObject):
    
    def __init__(self):
        super(ManageTheadSustain, self).__init__()
        
        self.ip_obj_list = []
        # self.ths = []
        self.ping_ip_list = []

        self.th_dict = {}
        self.pingip_dict = {}
        
    
    def __cr_one_thread(self, ip_obj: IpState):
        if ip_obj.state == IpState.STATE_FINDED:
            logger.warning("crate thread fail, beause ip status is <finded>")
            return
        
        one_th = QThread()
        ping_ip_obj = PingIp3While(ip_obj)
        ping_ip_obj.moveToThread(one_th)
        one_th.started.connect(ping_ip_obj.run)
        # self.ths.append(one_th)
        self.th_dict.setdefault(ip_obj.ip, one_th)
        # self.ping_ip_list.append(ping_ip_obj)
        self.pingip_dict.setdefault(ip_obj.ip, ping_ip_obj)
        
    def add_ip_obj(self, ip_obj: IpState):
        """
       添加一个IPState对象, 如果列表中存在着和它ip相同的对象,则不再添加;
        并创建一个线程
        
        如果一个IpState的状态是 STATE_FINDED,则不添加
        """
        for in_ip_obj in self.ip_obj_list:
            if ip_obj.is_ip_equal(in_ip_obj):
                break
        else:
            if ip_obj.state == IpState.STATE_FINDED:
                return None
            else:
                self.ip_obj_list.append(ip_obj)
                self.__cr_one_thread(ip_obj)
    
        
        
    def create_threads(self, ips):
        logger.debug("创建所有的线程")
        if isinstance(ips, IpState):
            self.add_ip_obj(ips)
        elif isinstance(ips, list):
            for ip in ips:
                self.add_ip_obj(ip)


    def quit_one(self, ipstate: IpState):
        """
        退出一个线程
        
        """
        th = self.th_dict.get(ipstate.ip, None)
        if th is None:
            return
        th.quit()
    
        pingobj: PingIp3While = self.pingip_dict.get(ipstate.ip)
        pingobj.set_stop()
        self.pingip_dict.pop(ipstate.ip)
        
    
    def start(self):
        logger.debug("开启所有线程")
        # for th in self.ths:
        #     th.start()

        for th in self.th_dict.values():
            th.start()

    def quit(self):
        logger.debug("退出所有的线程")

        for th in self.th_dict.values():
            th.quit()
            logger.debug("th is quit, and it value is : {}".format(th))

        for ping_ip_obj in self.pingip_dict.values():
            ping_ip_obj.set_stop()

    def __len__(self):
        return len(self.ths)
    
    
    


    


if __name__ == '__main__':
    pass
