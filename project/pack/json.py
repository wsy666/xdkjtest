# coding:utf8

import json
import os


class Json:
    def __init__(self, jsonpath):
        if not os.path.exists(jsonpath):
            # raise ValueError("Json文件:{}, 路径不存在".format(jsonpath))
            # 如果文件不存在,则创建一个文件
            with open(jsonpath, encoding='utf8', mode='w+'):
                pass
        self.path = jsonpath
    
    def load(self):
        """
        从json文件中读取对象
        """
        with open(self.path, mode='r', encoding='utf8') as f:
            return json.load(f)
    
    def dump(self, obj):
        """
        把一个对象下载到文件
        这个对象必须是以下类型之一:
        字典, 列表,元组, int, float, str类型
        """
        with open(self.path, encoding='utf8', mode='w') as f:
            json.dump(obj, f, ensure_ascii=False, indent=4)
    
    def dump_instance(self, instance):
        """
        
        """
        self.dump(instance.__dict__)
    
    def load_instance(self, classtype):
        dt = self.load()
        obj = classtype()
        obj.__dict__.update(dt)
        return obj
    
    
    @staticmethod
    def get_class_attr(dict_class: dict):
        
        """
        获取一个类的属性
        dict_class: 类名.__dict__
        """
        dt = {}
        for key in dict_class:
            if key.startswith('__'):
                continue
            
            value = dict_class.get(key)
            if hasattr(value, '__call__'):
                continue
            
            dt.setdefault(key, value)
        
        return dt
    
    def dump_class(self, classtype):
        """
        下载class的属性(除了__双下划线的属性和类方法)到json文件
        """
        dt = self.get_class_attr(classtype.__dict__)
        self.dump(dt)
        
    
    def load_class(self, classtype):
        """
        input a class type , 比如class A
        从文件加载其属性
        """
        dt = self.load()
        # print (type(classtype.__dict__))  不是一个dict类型
        for attr in dt:
            value = dt.get(attr)
            setattr(classtype, attr, value)
        
        
        
