# coding:utf8

from log import logger
import ctypes, sys

import os
import re
import socket
from time import sleep

# import psutil
import wmi
from wmi import WMI

from project import f_check_ip
from ...pack.config import JsonSetPc

# import sys
# import win32com.shell.shell as shell
# ASADMIN = 'asadmin'
#
# if sys.argv[-1] != ASADMIN:
#     script = os.path.abspath(sys.argv[0])
#     params = ' '.join([script] + sys.argv[1:] + [ASADMIN])
#     shell.ShellExecuteEx(lpVerb='runas', lpFile=sys.executable, lpParameters=params)
#     # sys.exit(0)

logger.info("")
set_ip_object = []
json_setpc = JsonSetPc()

path = 'ips.json'

class Card(object):
    """
    一个网卡对象
    
    """
    
    def __init__(self, card: wmi._wmi_object = None):
        """ """
        self.card = card
    
    # def get_name(self):
    #     return self.card.Description
    pass
    
    @property
    def name(self):
        """
        返回网卡的名字
        :return  "card_name" or ""
        
        """
        if self.card is None:
            return  ""
        return self.card.Description
    
    def index(self):
        return self.card.Index
    
    
    def ips(self):
        """
        获得所有的IP集合
        :return
            [ipstr...]
        """
        ips: set = self.card.IPAddress
        return self._filter_ip(ips)
    
    def _filter_ip(self, ips: list):
        """
        过滤符合IP格式的字符串
        :return [] or [ip_str....]
        """
        filter_ips = []
        if ips:
            for ip in list(ips):
                if f_check_ip(ip):
                    filter_ips.append(ip)
        return filter_ips
    
    def gateway(self):
        """
        返回默认网关

        """
        if self.card.DefaultIPGateway:
            return list(self.card.DefaultIPGateway)[0]
        pass
    
    def mac(self):
        """
        获得此网卡的mac地址

        """
        ips = self.card.IPAddress
        if ips:
            ips = list(ips)
            return ips[-1]
    
    def ip_subnet(self):
        """
        获得所有的掩码

        """
        subnets = self.card.IPSubnet
        return self._filter_ip(subnets)
    
    def ip_subnet_tuples(self):
        """
        :return  返回[(ipstr, 掩码)...]的zip对象
        
        """
        ips = self.ips()
        subnets = self.ip_subnet()
        if len(ips) == len(subnets) and ips:
            return zip(ips, subnets)
    
    def set_ip_and_mask(self, ips: list, masks: list):
        """
        设置IP和掩码
        
        """
        result = self.card.EnableStatic(IPAddress=ips, SubnetMask=masks)
        if result[0] == 0 or result[0] == 1:
            print("设置ip成功!")
            return True
        else:
            print("设置IP不成功~~")
            return False
    
    def set_gateway(self, interway):
        """
        设置网关
        
        """
        if interway:
            wayres = self.card.SetGateways(DefaultIPGateway=interway, GatewayCostMetric=[1])
            if wayres[0] == 0:
                return True
    
    def set_dns(self, dns):
        # 修改dns
        if dns:
            dnsres = self.card.SetDNSServerSearchOrder(DNSServerSearchOrder=dns)
            if dnsres[0] == 0:
                print('设置DNS成功,等待3秒刷新缓存')
                sleep(3)
                # 刷新DNS缓存使DNS生效
                os.system('ipconfig /flushdns')
            else:
                print('修改DNS失败')
                return False




def write():
    json_setpc.set(set_ip_object)
    pass

def read():
    set_ip_object = json_setpc.get()
    pass

def set_ips_and_masks():
    print("in set_ips_and_masks()....")
    if set_ip_object:
        ips, masks, card_text = set_ip_object
        print('in set_ips_and_masks(): {}'.format(ips, masks))
        card = obj_network.get_card_from_name(card_text)
        result = card.card.EnableStatic(IPAddress=ips, SubnetMask=masks)
        print(result)
        # todo:写法修改
        if result[0] == 0 or result[0] == 1:
            logger.info("增加本机IP成功")
        else:
            logger.warning("增加本机IP失败")
            raise ValueError("设置失败")

def __set_pc_ips(ips, card: Card):
    """
    设置本机的ip列表, 掩码默认为255.255.255.0
    :return
        None: 失败
    """
    if not isinstance(ips, (tuple, list)) :
        return
    
    masks = ["255.255.255.0"] * len(ips)
    
    # logger
    logger.debug("将要添加如下IP 和 子网掩码: ")
    for ip, mask in zip(ips, masks):
        logger.debug("+ ip: {}, 掩码: {}".format(ip, mask))
        
    result = card.card.EnableStatic(IPAddress=ips, SubnetMask=masks)
    if result[0] in (0, 1):
        logger.info("增加本机IP成功")
        return True
    else:
        logger.warning("增加本机IP不失败")
        
        
def set_pc_ips(ips, card: Card):
    get_admin_and_do(__set_pc_ips, ips=ips, card=card)
    

def inner():
    """
    获取管理员权限
    """
    if ctypes.windll.shell32.IsUserAnAdmin():
        return
    else:
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, __file__, None, 0)
        # sys.exit()


class NetWorkCard(object):
    __instance =None
    __init__flg = False
    
    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = object.__new__(cls)
            # cls.__init__(*args, **kwargs)
            # cls.__init__flg = True
        
        return cls.__instance
        
        
    def __init__(self):
        """ """
        if self.__init__flg == True:
            logger.info(f"{self.__class__.__name__} 已经初化过,不再初始化了")
            return
        self.w = WMI()
        self.configs = self.w.Win32_NetworkAdapterConfiguration(IPEnabled=True)
        # 网卡的列表
        self.cards: list = [Card(net) for net in self.configs]
        self.defaultCard = None
        
        self.__class__.__init__flg = True
        
        
    
    def get_card_names(self):
        """
        获得所有网卡的名字

        """
        for net in self.configs:
            yield net.Description
    
    def get_card_from_name(self, name="环回"):
        """
        如果self.cards为空 则返回
        如果根据名字没有找到相应的card,则返回第一个card
        :return 1 None 2 card object
        """
        if not self.cards:
            return
        
        for card in self.cards:
            if name in card.name:
                return card
        else:
            return self.cards[0]
    
    def get_card_from_index(self, index):
        for card in self.cards:
            if index == card.index():
                return card




# def get_ips(name_wang_ka):
#     ips = []
#     for name, info in psutil.net_if_addrs().items():
#         if name_wang_ka in name:
#             for addr in info:
#                 if socket.AddressFamily.AF_INET == addr.family:
#                     ips.append(addr.address)
#         else:
#             continue
#     return ips if ips else None


# def get_wangka_info():
#     w = WMI()
#     data = {}
#     count = 0
#     for nic in w.Win32_NetworkAdapterConfiguration():
#         if nic.MACAddress is not None:
#             count += 1
#             iter_data = {}
#             iter_data['macaddress'] = nic.MACAddress
#             iter_data['model'] = nic.Caption
#             iter_data['name'] = nic.Index
#
#             if nic.IPAddress is not None:
#                 iter_data['ipaddress'] = nic.IPAddress[0]
#                 iter_data['netmask'] = nic.IPSubnet
#             else:
#                 iter_data['ipaddress'] = ''
#                 iter_data['netmask'] = ''
#             data["nic%s" % count] = iter_data
#     return data


# def set_wangka_ip(ip, mask, index):
#     w = WMI()
#     # confs = w.Win32_NetworkAdapterCinfiguration(IPEnabled=True)
#     confs = w.Win32_NetworkAdapterConfiguration(IPEnabled=True)  # 获取到本地所有有网卡信息,list
#     print("confs 的长度位  {}".format(len(confs)))
#     if index <= len(confs):
#         conf = confs[index]
#         result = conf.EnableStatic(IPAddress=ip, SubnetMask=mask)
#         print(result)
#         if result[0] == 0 or result[0] == 1:
#             print("设置ip成功!")
#         else:
#             print("设置IP不成功~~")


"""
class UpdateIp(object):
    re_ip_str = r"\d+.\d+.\d+.\d+"
    
    def __init__(self):
        self.wmiservice = WMI()
        self.configs = self.wmiservice.Win32_NetworkAdapterConfiguration(
            IPEnabled=True  # # 获取到本机所有的网卡的信息,list
        )
    
    def get_inter(self):
        """ """
        flag = 0
        # 遍历所有网卡，找到要修改的那个
        for con in self.configs:
            ip = re.findall(self.re_ip_str, con.IPAddress[0])
            if len(ip) > 0:
                return 0
            else:
                flag = flag + 1
        return flag
    
    def runset(self, ip, subnetmask, interway=None, dns=None):
        adapter = self.configs[self.get_inter()]
        
        # 开始执行修改ip、子网掩码、网关
        ipres = adapter.EnableStatic(IPAddress=ip, SubnetMask=subnetmask)
        if ipres[0] == 0:
            print('设置IP成功')
        else:
            if ipres[0] == 1:
                print('设置IP成功，需要重启计算机！')
            else:
                print('修改IP失败')
                return False
        
        # 修改网关
        if interway:
            wayres = adapter.SetGateways(DefaultIPGateway=interway, GatewayCostMetric=[1])
            if wayres[0] == 0:
                print('设置网关成功')
            else:
                print('修改网关失败')
                return False
        
        # 修改dns
        if dns:
            dnsres = adapter.SetDNSServerSearchOrder(DNSServerSearchOrder=dns)
            if dnsres[0] == 0:
                print('设置DNS成功,等待3秒刷新缓存')
                sleep(3)
                # 刷新DNS缓存使DNS生效
                os.system('ipconfig /flushdns')
            else:
                print('修改DNS失败')
                return False
"""

def get_admin_and_do(do_function, *args, **kwargs):
    """
    获取管理员权限后再执行操作函数
    :return
        False : 没有获取到管理员权限
    """
    
    def is_admin():
        try:
            return ctypes.windll.shell32.IsUserAnAdmin()
        except:
            print("False")
            return False
    
    if is_admin():
        # Code of your program here
        do_function(*args, **kwargs)
        # input('')
    else:
        # Re-run the program with admin rights
        print("re-run the program with admin rights ")
        # ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, " ".join(sys.argv), None, 1)
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, __file__, None, 1)


obj_network = NetWorkCard()
card = obj_network.get_card_from_name()
logger.debug("NetWorkCard()")

if __name__ == '__main__':
    read()
    get_admin_and_do(set_ips_and_masks)
    
    # todo 删除一些IP
    # todo 网卡根据名字来差早;
    # todo 长传忽略文件
    pass
